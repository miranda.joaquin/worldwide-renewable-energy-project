import os
import pandas as pd
import geopandas as gpd
import tkinter as tk
import matplotlib.pyplot as plt
from tkinter import filedialog
from tkinter import simpledialog

"""The link between a cvs file with the data to plot a map, and the assignation of polygons to limit a country 
is the file 'wikipedia-iso-country-codes.csv' codes:csv, which contains the codes, ISO-codes, of each country."""

df_countries = pd.read_csv('wikipedia-iso-country-codes.csv')  # File with the codes assigned to countries

# Mapping function
"""Main function to create a map. It uses Pandas, Geopandas and Matplotlib"""

def plotmaps(folder, tittle, tochange, color):
    indir = os.getcwd()
    indir = indir + folder
    fname = filedialog.askopenfilename(filetypes=[("csv files", "*.csv")], initialdir=indir, title="Select File:")

    head, exten = fname.split(".", 1)
    head, year = head.split("_", 1)
    tittle = tittle + year

    plt.ion()                                                      # Allows to keep several plots simultaneously opened
    df_global = pd.read_csv(fname)                                 # Read with Panda the data csv file.
    df_global = df_global.merge(df_countries, how='left', left_on=['Entity'],  # Merge the data from 2 files in one
                                right_on=['English short name lower case'])
    df_global.rename(columns={tochange: 'info', 'English short name lower case': 'Nation'},
                     inplace=True)                                  # For convinience  change column's name
    df_global = df_global[
        ['Entity', 'Code', 'Year', 'info', 'Nation', 'Alpha-2 code', 'Alpha-3 code', 'Numeric code',
         'ISO 3166-2']]

    """Geopandas provides the support to create maps by allocating polygons to the shape of a country. 
    This is done in the next, where the variable world is assigned."""

    world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))  # Get poligons to plot maps from geopandas
    print(world.columns.values)
    print(world[world['continent'] == 'Antarctica'])
    world = world.drop(world.index[159])                                  # Remove Antarctica from Map



    mapped = world.merge(df_global[['Code', 'info']], how='left', left_on='iso_a3', right_on='Code')  # Merge data
    mapped = mapped.fillna(0)                                    # Fill with zero unavailable data

    to_be_mapped = 'info'                                        # Selection of data to be mapped
    vmin, vmax = 0, df_global['info'].max()                      # Search of max and min in data
    fig, ax1 = plt.subplots(1, figsize=(13, 8))                  # Set a plot and its size troughout Matplotlib
    mapped.plot(column=to_be_mapped, cmap=color, linewidth=0.8, ax=ax1, edgecolors='0.8')  # Set a plot for the map
    ax1.set_title(tittle, fontdict={'fontsize': 28})             # Tittle displayed as header in figure.
    ax1.set_axis_off()                                           # Instruction to not display the axis

    sm = plt.cm.ScalarMappable(cmap=color, norm=plt.Normalize(vmin=vmin, vmax=vmax))  # Matplot builtin colormaps,
    cbar = fig.colorbar(sm, orientation='horizontal')            # Set the colormap scale bar
    plt.tight_layout()
    plt.show()


# Pre-mapping function
"""The call to the function mapp from main.py will create a tk child named top_child which  selects files"""
def mapp(folder, tittle, tochange, color, f_father):

    top_child = tk.Toplevel(f_father)                            # Submenu with buttons
    top_child.title("More Maps")
    # top_child.geometry("280x50")
    my_str1 = tk.StringVar()
    l1 = tk.Label(top_child, textvariable=my_str1)
    l1.grid(row=1, column=0, sticky=tk.E + tk.W, ipadx=32)
    my_str1.set(tittle)

    bc1 = tk.Button(top_child, text="Choose year", command=lambda: plotmaps(folder, tittle, tochange, color))
    bc1.bind("<Button-2>", lambda event: plotmaps(folder, tittle, tochange, color))
    bc1.grid(row=2, column=0, sticky=tk.E + tk.W, ipadx=82)

    bc2 = tk.Button(top_child, text="  Quit  ", command=top_child.destroy)  # Closing Button
    bc2.grid(row=5, column=0, sticky=tk.E + tk.W)

# Make curves for n countries
"""The second submenu, located in the second bottom half of the main menu, contains buttons that allow plotting in
single figure information for a n number of countries. After clicking any of the four buttons of this sub-menu, the
class Curves creates in sequence two dialog windows to enter information. The first window asks for given the
number of countries to plot in a single"""

class Curves:

    def __init__(self, file, toplot, ylabel):
        self.file = file
        self.toplot = toplot
        self.ylabel = ylabel

        plt.ion()
        listcountries = []                                 # Create a list to place the countries.
        indir = os.getcwd()  # Obtain the current path and provide full path to the folder where the data is (next line)
        indir = indir + "/" + file
        df_cumulative_all = pd.read_csv(indir)             # Define, after reading with pandas, the data of the csv file
        # print(df_cumulative_all.columns.tolist())        # Check the head of the columns.

        ncount = simpledialog.askinteger("Number", "Enter the number of countries to plot")

        count = 0                                           # Initialize counter to be use in while-loop.

        while count < ncount:                               # Enter the 4 countries. Condition to stop entering after 4
            count = count + 1
            country = simpledialog.askstring("Names", f"Enter the names of {ncount} countries one by one")
            print(country)
            listcountries.append(country)                   # Append to list of countries

        # print(df_cumulative[df_cumulative.Entity == country].head())

        # plt.title("Installed Wind Energy Capacity")
        fig2 = plt.figure()
        ax2 = fig2.add_subplot(111)

        for i in range(0, ncount):              # Loop to create a curve for each entered country
            tmp = df_cumulative_all[df_cumulative_all.Entity == listcountries[i]]
            plt.plot(tmp['Year'], tmp[toplot], label=listcountries[i])

        year_max = tmp["Year"].max()            # Take the data from the last country and search for max and min values
        year_min = tmp["Year"].min()

        ax2.set_xlabel("Year")                  # Next 8 lines are settings and decorations for the plot
        ax2.set_ylabel(ylabel)
        ax2.set_xlim(year_min, year_max)
        ax2.set_ylim(ymin=-1)
        plt.grid(axis='y')
        ax2.legend()
        plt.tight_layout()
        plt.show()
